import {StackNavigator,DrawerNavigator,SafeAreaView,DrawerItems} from 'react-navigation'
import React, { Component } from 'react';
import {connect} from 'react-redux'
import {View,ScrollView,Text,StyleSheet,TouchableOpacity} from 'react-native'
import {Splash} from '../components/spalsh'
import Dashboard from '../components/dashboard'
import Income from '../components/income'
import Expenses from '../components/expenses'
import Form from '../components/form'






export const Routes = StackNavigator({
  home: {
    screen: Splash,
  },
  dashboard: {
    screen: Dashboard,
  },
  income: {
    screen: Income
  },
  expenses: {
    screen: Expenses
  },
  form: {
    screen: Form
  }
},
  {
    headerMode: "none"
  }
)

