import React ,{Component} from 'react'
import {Image,View,Text,TextInput,TouchableOpacity,ScrollView,Dimensions,Keyboard,AsyncStorage,Button, Vibration} from 'react-native'
import firebase from 'firebase'
import ModalDropdown from "react-native-modal-dropdown";

const {height,width} = Dimensions.get("window") 


class Form extends Component{
    constructor(){
        super();
        this.state={
            keyboardOpened:false,
            Month:["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][new Date().getMonth()],
            Year:new Date().getFullYear().toString(),
            by:"",
            category:"",
            cost:""
        }
    }

    componentWillMount () {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
    }

    componentWillUnmount () {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow () {
        this.setState({ keyboardOpened: true })
    };

    _keyboardDidHide () {
        this.setState({ keyboardOpened: false })
    };


    save(){
        AsyncStorage.getItem(this.props.navigation.state.params.type, (err, result) => {
            result = JSON.parse(result)
            if (result && Array.isArray(result)) {
                result = result.concat({Month:this.state.Month,Year:this.state.Year,category:this.state.category,by:this.state.by,cost:this.state.cost})
            } else {
                result = [{Month:this.state.Month,Year:this.state.Year,category:this.state.category,by:this.state.by,cost:this.state.cost}]
                
            }

            AsyncStorage.setItem(this.props.navigation.state.params.type, JSON.stringify(result), () => {

            });
        }
    ).then(()=>this.setState({category:"",by:"",cost:""}))
    }
    render(){
        return(
            <View style={{flexDirection:"column"}}>


<View style={{width:width,backgroundColor:"rgb(1,37,71)",height:50,alignItems:"center",alignContent:"center",justifyContent:"space-between",flexDirection:"row"}}
            >
            <Text style={{fontSize:30 ,color:"white"}} onPress={()=>this.props.navigation.goBack()}>{"<"}</Text>
            <Text style={{color:"white",fontSize:20,fontFamily:"Arial",fontStyle:"italic",fontWeight:"bold"}}>Add {this.props.navigation.state.params.type}</Text>
            <Text></Text>
            </View>
            <ScrollView >
                {this.state.keyboardOpened || <View style={{height:60,width:width}}></View>}
                <View style={{height:40,width:width}}></View>
                <View style={{flexDirection:"row" , marginRight:8,marginLeft:8,justifyContent:"space-between"}}>
                <Text style={{color:"rgb(1,37,71)"}}>Month/Year</Text>

                    <View>
                        <ModalDropdown 
                            options={
                                ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
                            }
                            onSelect={(idx, value) => this.setState({ Month: value })}
                            dropdownStyle={{width:120}} dropdownTextStyle={{color:"rgb(1,37,71)",fontSize:24}}
                            >
                            <View style={{width:120,borderWidth:0.75, backgroundColor: "rgb(1,37,71)",borderRadius:5}}><Text style={{color:"white",fontSize:24}}>{this.state.Month}</Text></View>
                        </ModalDropdown>
                    </View>

                    <View>
                        <ModalDropdown
                            options={["2013","2014","2015","2016","2017","2018","2019","2020","2021","2022","2023"]}
                            onSelect={(idx, value) => this.setState({ Year: value })}
                            dropdownStyle={{width:120}} dropdownTextStyle={{color:"rgb(1,37,71)",fontSize:24}}
                        >
                            <View style={{width:120,borderWidth:0.75, backgroundColor: "rgb(1,37,71)",borderRadius:5 }}><Text style={{color:"white",fontSize:24}}>{this.state.Year}</Text></View>
                        </ModalDropdown>
                    </View>
                </View>

                    <View style={{height:30}}></View>

                    <View style={{flexDirection:"row", marginRight:8,marginLeft:8,justifyContent:"space-between"}}>
                    <Text style={{color:"rgb(1,37,71)",fontSize:16}}>Category</Text>
                    <View style={{  borderRadius:10,
                                    height:35,
                                    width:width*0.715,
                                    backgroundColor:"rgb(1,37,71)"}}
                    >
                    <TextInput style={{color:"white"}} underlineColorAndroid="transparent" value={this.state.category} onChangeText={(text) => this.setState({ category: text })} />
                    </View>
                    </View>

                    <View style={{height:30}}></View>

                    <View style={{flexDirection:"row", marginRight:8,marginLeft:8,justifyContent:"space-between"}}>                    
                    <Text style={{color:"rgb(1,37,71)",fontSize:16}}>By</Text>
                    <View  style={{  borderRadius:10,
                                    height:35,
                                    width:width*0.715,
                                    backgroundColor:"rgb(1,37,71)"}}
                    >
                    <TextInput style={{color:"white"}} underlineColorAndroid="transparent" value={this.state.by} onChangeText={(text) => this.setState({ by: text })} />
                    </View>
                    </View>

                    <View style={{height:30}}></View>

                    <View style={{flexDirection:"row", marginRight:8,marginLeft:8,justifyContent:"space-between"}}>                    
                    <Text style={{color:"rgb(1,37,71)",fontSize:16}}>cost</Text>
                    <View  style={{  borderRadius:10,
                                    height:35,
                                    width:width*0.715,
                                    backgroundColor:"rgb(1,37,71)"}}
                    >
                    <TextInput style={{color:"white"}} keyboardType = "numeric" underlineColorAndroid="transparent" value={this.state.cost} onChangeText={(text) => this.setState({ cost: text })} />
                    </View>
                    </View>
                    </ScrollView>
                    <View style={{height:height*0.24}}></View>
                    {this.state.keyboardOpened || 
                // <Button title="Add" onPress={this.save.bind(this)} />
                        <View style={{  height:height*0.12,
                                        width:width,
                                        backgroundColor:"rgb(1,37,71)",
                                        justifyContent:"center",
                                        flexDirection:"row"
                                    }}><TouchableOpacity onPress={this.save.bind(this)} disabled={!(this.state.by.length && this.state.category.length && this.state.cost.length)}><Text style={{color:"white",fontSize:25}}>Add</Text></TouchableOpacity></View>
                    }
            </View>
        )
    }
}
export default Form