import React ,{Component} from 'react'
import {View,TouchableOpacity,Text,Image,AsyncStorage,Dimensions} from 'react-native'
import firebase from 'firebase'
import ModalDropdown from "react-native-modal-dropdown";

const {height,width} = Dimensions.get("window") 

 
class Income extends Component{
    constructor(){
        super();
        this.state={
                        Month:["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][new Date().getMonth()],
                        Year:new Date().getFullYear().toString()
                    }
            
    }
    componentDidMount(){
        AsyncStorage.getItem("Income", (err, result) => {
            result && this.setState({
                Data: JSON.parse(result).map((data) => {
                    if (data && data.Month === this.state.Month && data.Year === this.state.Year) { return data }
                }
                )
            })
        }
        )
    }
    render(){//244*3 ,255*3 ,195*3
        var sum = 0;
        this.state.Data?        this.state.Data.filter((data)=>{if(data!=undefined){return sum += parseFloat(data.cost)}}) : console.log("ponka")
        var i = 0;
        return(
            <View>

                <View style={{ width: width, backgroundColor: "rgb(1,37,71)", height: 50, alignItems: "center", alignContent: "center", justifyContent: "space-between", flexDirection: "row" }}
                >
                    <Text style={{ fontSize: 30, color: "white" }} onPress={() => this.props.navigation.goBack()}>{"<"}</Text>
                    <Text style={{ color: "white", fontSize: 30, fontFamily: "Arial", fontStyle: "italic", fontWeight: "bold" }}>Income</Text>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate("form", { type: 'Income' })}><Image source={require("./formRoute.png")} style={{ height: 40, width: 40 }} /></TouchableOpacity>
                </View>



                <Text></Text>
                <View style={{flexDirection:"row" , marginRight:8,marginLeft:8,justifyContent:"space-between"}}>
                {/* <Text></Text> */}
                <Text></Text>
                    <View>
                        <ModalDropdown
                            options={
                                ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
                            }
                            onSelect={(idx, value) => {
                                this.setState({ Month: value });
                                // AsyncStorage.getItem("Expenses", (err, result) => this.setState({ Data: JSON.parse(result) }))
                                AsyncStorage.getItem("Income", (err, result) => this.setState({Data:JSON.parse(result).map((data)=>{if(data && data.Month === this.state.Month && data.Year === this.state.Year){return data}})}))
                                
                            }}
                            dropdownStyle={{ width: 80 }} dropdownTextStyle={{ color: "rgb(1,37,71)", fontSize: 24 }}
                        >
                            <View style={{ width:120,borderWidth:0.75, backgroundColor: "rgb(1,37,71)",borderRadius:5 }}><Text style={{ color:"white",fontSize:24 }}>{this.state.Month}</Text></View>
                        </ModalDropdown>
                    </View>

                    <View>
                        <ModalDropdown
                            options={["2013","2014","2015","2016","2017","2018","2019","2020","2021","2022","2023"]}
                            onSelect={(idx, value) => {this.setState({ Year: value });
                    AsyncStorage.getItem("Income", (err, result) => this.setState({Data:JSON.parse(result).map((data)=>{if(data && data.Month === this.state.Month && data.Year === this.state.Year){return data}})}))
        
                        }}
                            dropdownStyle={{ width: 80 }} dropdownTextStyle={{ color: "rgb(1,37,71)", fontSize: 24 }}
                        >
                            <View style={{width:120,borderWidth:0.75, backgroundColor: "rgb(1,37,71)",borderRadius:5}}><Text style={{ color:"white",fontSize: 24 }}>{this.state.Year}</Text></View>
                        </ModalDropdown>
                    </View>
            <Text></Text>
                </View>

                        <View style={{height:20}}></View>

                <View style={{flexDirection:"row",justifyContent:"center"}}>
                    <View style={{height:height*0.6,width:width*0.9,borderRadius:10,backgroundColor:"white"}}>
                <View style={{  backgroundColor:"rgb(255,255,255)",
                                borderTopLeftRadius:10,
                                borderTopRightRadius:10,
                                flexDirection:"row",
                                justifyContent:"space-between",
                                height:35,
                                paddingRight:10,
                                borderBottomColor:"rgb(1,37,71)",
                                borderBottomWidth:1.2,
                                paddingLeft:10,
                                
                                }}
                ><Text style={{color:"rgb(1,37,71)",fontSize:24}}>Category</Text><Text style={{fontSize:24,color:"rgb(1,37,71)"}}>By</Text><Text style={{fontSize:24,color:"rgb(1,37,71)"}}>Cost</Text></View>
                    {this.state.Data ? this.state.Data.map((data,index)=>{if(data!=undefined){i++
                        return<View key={index} style={{  backgroundColor:i%2!=0? "rgb(244,244,244)" : "rgb(195,195,195)",
                                            height:50,
                                            flexDirection:"row",
                                            justifyContent:"space-between",
                                            paddingRight:10,
                                            paddingLeft:10}}>
                        <Text style={{color:"rgb(1,37,71)",fontSize:20}}>{data.by}</Text>
                        <Text style={{color:"rgb(1,37,71)",fontSize:20}}>{data.category}</Text>
                        <Text style={{color:"rgb(1,37,71)",fontSize:20}}>{data.cost}</Text>
                        </View>
                    }})
            :
            <Text>No data </Text>
        } 
        </View>                    
                </View> 
                <View style={{height:20}}></View>
            <View style={{  marginHorizontal:width*0.05,
                            borderRadius:15,
                            flexDirection:"row",
                            justifyContent:"space-between",
                            alignItems:"center",
                            backgroundColor:"rgb(1,37,71)",
                            height:65,
                            paddingRight:10,
                            paddingLeft:10,
                            width:width*0.9
                            }}>
            <Text></Text>
            <Text style={{fontSize:27,color:"white"}}>Total</Text>
            <Text style={{fontSize:27,color:"white"}}>{sum}</Text>
            </View>
            </View>
        )
    }
}
export default Income



