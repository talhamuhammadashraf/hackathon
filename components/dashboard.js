import React ,{Component} from 'react'
import {View,Text,TouchableOpacity,AsyncStorage,DatePickerAndroid,Dimensions,Image} from 'react-native'

const {height,width} = Dimensions.get("window") 
class Dashboard extends Component {
    constructor(){
        super();
        this.state={Year:"2017"}
    }

componentDidMount(){
    AsyncStorage.getItem("Income", (err, result) => {
        var Income=JSON.parse(result)
        Income=Income.filter((value)=>{if(value.Year==this.state.Year){return value}})
        console.log(Income,"this is result")
    })

    AsyncStorage.getItem("Expenses", (err, result) => {
        var Expenses=JSON.parse(result)
        Expenses=Expenses.filter((value)=>{if(value.Year==this.state.Year){return value}})

        console.log(Expenses,"this is result")    })
        
        
}

    render() {
        return (
            <View style={{backgroundColor:"rgb(210,209,210)",width:width,height:height}}
            // style={{ backgroundColor: "rgb(1,37,71)" }}
            >
            <View style={{width:width,backgroundColor:"rgb(1,37,71)",height:50,alignItems:"center",alignContent:"center"}}
            >
            <Text style={{color:"white",fontSize:30,fontFamily:"Arial",fontStyle:"italic",fontWeight:"bold"}}>Dashboard</Text>
            </View>

                <View style={{
                    marginTop: height * 0.5,
                    flexDirection: "row",
                    justifyContent: "space-between"
                    // marginLeft:width*0.19
                }}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate("income")}><Image source={require("./income.png")}  style={{ marginLeft: 40 }} /></TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate("expenses")}><Image source={require("./exp.png")}  style={{ marginRight: 40 }}/></TouchableOpacity>
                </View>
            </View>
        )
    }
}

export default Dashboard