/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
} from 'react-native';

import {Routes} from './navigation/Router'
import {Provider,connect} from 'react-redux'
import store from './store';
import {addNavigationHelpers} from 'react-navigation'

class AppWithNavigationState extends Component<{}> {
  render() {
    return(
      // <View>
      //   <Text>hi this is app</Text>
      // </View>
      <Routes navigation={
        addNavigationHelpers({
          dispatch:this.props.dispatch,
          state:this.props.navReducer
        })
      }
      />
    );
  }
}

const mapStateToProps=(state)=>({navReducer:state.navReducer})
const NavigationRedux=connect(mapStateToProps)(AppWithNavigationState)
export default class App extends Component{
  render(){
    return(
      <Provider store={store}>
      <NavigationRedux/>
      </Provider>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  }
});
