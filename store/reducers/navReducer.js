import { addNavigationHelpers } from 'react-navigation';

import {Routes} from '../../navigation/Router'
// const initialState = Routes.router.getStateForAction(Routes.router.getActionForPathAndParams('Login'));

export const navReducer = (state, action) => {
  const nextState = Routes.router.getStateForAction(action, state);

  // Simply return the original `state` if `nextState` is null or undefined.
  return nextState || state;
};
