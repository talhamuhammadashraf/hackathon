import {combineReducers} from "redux"
import {navReducer} from './navReducer'

const Reducer=combineReducers({
    navReducer
})
export default Reducer