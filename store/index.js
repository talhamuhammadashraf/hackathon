import {createStore} from 'redux';
import Reducer from './reducers'
import { applyMiddleware } from 'redux';
import thunk from 'redux-thunk'
import logger from 'redux-logger'

const store=createStore(Reducer,applyMiddleware(logger,thunk))

export default store